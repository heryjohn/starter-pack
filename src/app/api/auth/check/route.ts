import { NextResponse } from 'next/server'

export async function GET (request: Request) {
  return NextResponse.json(false)
}

// If `OPTIONS` is not defined, Next.js will automatically implement `OPTIONS` and  set the appropriate Response `Allow` header depending on the other methods defined in the route handler.
export async function OPTIONS (request: Request) {}
