import type { NextApiRequest } from 'next'
import { NextResponse } from 'next/server'

export async function POST (
  request: Request,
  context: { params: { pid: string } }
) {
  console.log(request.body)
  return NextResponse.json(request.body)
}
