import { NextRequest, NextResponse } from 'next/server'

export async function GET (
  request: NextRequest & { query: Record<string, any> },
  context: { params: { model: string } }
) {
  const { searchParams } = new URL(request.url)
  const query: Record<string, any> = {}

  for (let el of [...(searchParams.entries() as unknown as [])]) {
    query[el[0]] = el[1]
  }
  return NextResponse.json({
    data: [],
    page: 0,
    lastPage: 0,
    total: 0,
    filters: {},
    params: context.params,
    query
  })
}

// If `OPTIONS` is not defined, Next.js will automatically implement `OPTIONS` and  set the appropriate Response `Allow` header depending on the other methods defined in the route handler.
export async function OPTIONS (request: Request) {}
