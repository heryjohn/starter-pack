import type { NextApiRequest } from 'next'
import { NextResponse } from 'next/server'

export async function GET (
  request: Request,
  context: { params: { id: string } }
) {
  return NextResponse.json(context.params)
}

export async function PUT (
  request: Request,
  context: { params: { id: string } }
) {
  return NextResponse.json(context.params)
}

export async function DELETE (
  request: Request,
  context: { params: { id: string } }
) {
  return NextResponse.json(context.params)
}
