export default function RootLayout ({
  children
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <div style={{ background: 'pink', color: 'black' }}>
      <h2>ADMIN</h2>
      {children}
    </div>
  )
}
