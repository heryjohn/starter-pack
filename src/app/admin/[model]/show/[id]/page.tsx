'use client'
import { useParams } from 'next/navigation'

export default function Show () {
  const params = useParams<{ model: string }>()
  return (
    <div>
      <h1>SHow ID page {JSON.stringify(params)}</h1>
    </div>
  )
}
