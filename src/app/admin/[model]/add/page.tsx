'use client'
import { useParams } from 'next/navigation'

export default function Add () {
  const params = useParams<{ model: string }>()
  return (
    <div>
      <h1>Add page {JSON.stringify(params)}</h1>
    </div>
  )
}
