'use client'
import { useParams } from 'next/navigation'

export default function Edit () {
  const params = useParams<{ model: string }>()
  return (
    <div>
      <h1>EDIT ID page {JSON.stringify(params)}</h1>
    </div>
  )
}
