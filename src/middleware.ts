import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'
// This function can be marked `async` if using `await` inside
export async function middleware (
  req: NextRequest & { query: Record<string, any> }
) {
  // showing the related code for your issue
  const { pathname, origin } = req.nextUrl

  const isLogged = await fetch(origin + '/api/auth').then(r => r.json())

  if (pathname.includes('/login') || isLogged) {
    // for those routes add your logic
    console.log({ pathname })
    // req.query = { rrr: 'name' }
    // req.headers.set('query', 'Custom-Value')
    return NextResponse.next()
  }
  return NextResponse.redirect(`${origin}/admin/login`)
}

// See "Matching Paths" below to learn more
export const config = {
  matcher: ['/admin/:path*', '/api/:path*']
}
